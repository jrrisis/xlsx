# Installation
> `npm install --save @types/xlsx`

# Summary
This package contains type definitions for xlsx (https://github.com/SheetJS/js-xlsx).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/types-2.0/xlsx

Additional Details
 * Last updated: Tue, 22 Nov 2016 23:38:11 GMT
 * File structure: ProperModule
 * Library Dependencies: none
 * Module Dependencies: none
 * Global values: none

# Credits
These definitions were written by themauveavenger <https://github.com/themauveavenger/>.
